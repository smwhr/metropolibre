<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AjaxController
{
  public function indexAction(Application $app, Request $request) {
    $params = $request->query->all();
    
    $values = array('a' => 1, 'b' => 'hello');
    
    return $app->json($values);
  }
  
}

