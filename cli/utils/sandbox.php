<?php
/**
 * Test
 */
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

$console->register('sandbox:test')
->setDescription('Do whatever')
->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
  $param      = $input->getArgument('param');

  $output->info(print_r($param, true));

})
->addArgument('param',InputArgument::OPTIONAL,'param')